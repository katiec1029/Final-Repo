/**
 *
 * Created by scarzer on 5/10/16.

 var container = document.getElementById('networkGraphColumn');
 var nodes = new vis.DataSet();
 var edges = new vis.DataSet();
 var data = {
    nodes : nodes,
    edges : edges
};
 var options = {
    interaction : {
        dragNodes : true,
        hover : true
    },
    edges: {
    },
    physics : false
};

 var network = new vis.Network(container, data, options);
 $.get('/api/all').then( (data) =>{
    for(var i = 0; i < data['node'].length; i++) {
        nodes.add({
            id: data['node'][i]['n.file'],
            label: data['node'][i]['name']
        })
    }
});
 */

$(function() {
    $.get('/api/all').then( (data) =>{
        // Pre allocate the array. Better performance in V8 due to optimizations
        var elements = [];
        for(var i = 0; i < 1000; i++){
            elements.push({
                'data': {
                    id: data['node'][i]['n.file'],
                    name: data['node'][i]['name']
                }
            })
        }

        var cy = window.cy = cytoscape({
            container: document.getElementById('networkGraphColumn'),

            boxSelectionEnabled: false,
            autounselectify: true,

            layout: {
                name: 'spread',
                minDist: 40
            },

            style: [
                {
                    selector: 'node',
                    style: {
                        'content': 'data(id)',
                        'font-size': 8,
                        'background-color': '#ea8a31'
                    }
                },

                {
                    selector: 'edge',
                    style: {
                        'curve-style': 'haystack',
                        'haystack-radius': 0,
                        'width': 3,
                        'opacity': 0.666,
                        'line-color': '#fcc694'
                    }
                }
            ],

            elements: elements
        })
    });
});

